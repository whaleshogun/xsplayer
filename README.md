# XSPlayer

A very compact synthesizer and sequencer written in Java. It uses neither samples nor MIDI but generates all sounds on the fly using mathematical formulas (additive synthesis). 

It supports all common waveforms and a number of effects such as low pass and high pass filters, EQ, delay, ring modulation etc.

The player and the demo song are packed into a 5KB Jar file, download a sample song [here](https://bitbucket.org/whaleshogun/xsplayer/downloads/xsplayer_demo_latest.jar) (double click to execute jar) or watch on Youtube:

[![](http://img.youtube.com/vi/d22tn1bae0w/0.jpg)](http://www.youtube.com/watch?v=d22tn1bae0w "Demo song")

## General features

* 16 bit stereo output (CD quality)
* 64 voice polyphony
* 64 track sequencer
* BPM setting
* Per track filters
* Arbitrary waveform construction in Java
* Score as java properties file
* Executable jar file only slightly larger than 5 KB
* Zero dependencies
* Tested with JDK 8, may work with other JDKs as well

## Built-in waveforms

* Sine
* Sawtooth
* Triangle
* Pulse / square (with pulse width modulation)
* Modulated white noise
* LFO
* Attack envelope
* Release envelope

## Built-in instruments

* Kickdrum
* Snare
* Hi-hat
* Short hi-hat
* Brassy bass
* Octaver lead
* Air pad
* Strings pad
* Plucked lead
* Acoustic guitar
* Flute lead
* Autopan bass effect

## Built-in per track filters

* Low pass filter
* High pass filter
* Delay
* Stereo width
* Stereo pan
* Hard limiter
* Ring modulator
* Click filter
* Waveshaper

## Master filters

* Tilt equalizer
* Hard limiter

## Song file format

The format is basically a Java property file. A sample song file (```1.sng```) is bundled with the source code. 
Commands start with a letter character and usually end with an integer or floating point value.

### Top level commands

| Command | Description | Range | Mandatory | Default |
|---------|------------------------------------------------------------------------------------------------------------------------------------|------------------|-----------|---------------|
| ```M``` | Master volume (keep it low to avoid clipping)| 0.0 ... | no | 1.0 |
| ```O``` | Tempo (BPM) | 1 ... | no | 140 |
| ```N``` | Time resolution, number of steps per beat (e.g. 16 means that the shortest note is 1/16 of a beat) | 1 ... | no | 16 |
| ```P``` | Play mode. Player repeats only the loop in looped mode. | 0: from start, ignore any loop 1: looped, see ```>``` and  ```<``` track level commands | no | 0 (from start) |
| ```Tn``` | List of blocks to play in track ```n```. Track starts looping when end reached. Comment line with ```#``` to mute track n. | see track level commands | yes |  |
| ```Bn``` | Step pattern for block ```n```. List of track level commands separated by whitespace. | see block level commands  | yes |  |

### Track level commands

These commands can be used inside a ```Tn``` top level command. 
Mandatory commands should be defined at the beginning of the track.
Repeat commands in track later on to change specific values (instrument, volume, etc.). 

| Command | Description | Range | Mandatory | Default |
|---------|------------------------------------------------------------------------------------------------------------------------------------|------------------|-----------|---------------|
| ```in``` | Instrument of current track | 0 ... 255  | yes |  |
| ```vn``` | Volume of current track (0.5 is a good start) | 0.0 ... | yes |  |
| ```ln``` | Low pass filter for current track. Positive values are interpreted as alpha, negative values as cutoff frequency (Hz). | 0.0 (max) ... 1.0 (off) or -22500 (off) ... -20 (max) | no | 1.0 (off) |
| ```hn``` | High pass filter for current track. Positive values are interpreted as alpha, negative values as cutoff frequency (Hz). | 0.0 (max) ... 1.0 (off) or -22500 (max) ... -20 (off) | no | 1.0 (off) |
| ```pn``` | Stereo pan for current track | -1.0 (left only) ... 1.0 (right only) | no | 0.0 (center) |
| ```wn``` | Stereo width for current track, the higher the wider | 0.0 (mono) ... | no | 0.0 (mono) |
| ```dn``` | Delay in seconds for current track | 0.0 (none) ... 1.0 (max) | no | 0.0 (none) |
| ```rn``` | Delay ratio for current track | 0.0 (off) ... 1.0 (max) | no | 0.0 (off)|
| ```bn``` | Play block n (see ```Bn``` top level command for block definition) | 0 ...  | yes |  |
| ```tn``` | Transpose following blocks by ```n``` halftones (+ or -)  | -120 ... 120 | no | 0 |
| ```en``` | Tilt EQ frequency (Hz) | 0 ... 20000 | no | 450 |
| ```gn``` | Tilt EQ gain (dB) | -6.0 (full low pass) ... 6.0 (full high pass) | no | 0 (off) |
| ```>``` | Loop start in looped mode, see ```P``` top level command  |  | no |  |
| ```<``` | Loop end in looped mode, see ```P``` top level command. If omitted, the song is played to the end.  |  | no |  |
| ```#``` | One word inline comment / label, e.g. ```#verse_1```, will be ignored  |  | no |  |
| ```q``` | End of tune, quit player  |  | yes |  |

### Block level commands

These commands can be used inside a ```Bn``` top level command. The commands are separated by whitespace.

| Command | Description | Range | Mandatory | Default |
|---------|------------------------------------------------------------------------------------------------------------------------------------|------------------|-----------|---------------|
| ```nx``` | Note length in the unit of ```N```, see above. E.g. ```x``` = 8 defines a note length of 8/16 (1/2) | 1 ...  | no | 1 |
| note | e.g. C4, A#3, B6 (the number is the octave) | [C-B][0-9]  | yes |  |
| ```-``` | continue playing last note |  | no |  |
| ```X``` | stop playing previous note |  | no |  |

### Instruments

Define your instruments in the Java Method ```getSampleFromMixer```.

## Notes

The code is optimized for class file (and jar file) size. 
To reduce size, one must take on unusual coding habits, such as using as little 
classes, imports, methods, objects and variables as possible. In the end the code looks
almost like C...