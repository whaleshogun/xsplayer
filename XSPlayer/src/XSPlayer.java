import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.exp;
import static java.lang.Math.log;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.Math.signum;
import static java.lang.Math.sin;

import java.util.Arrays;
import java.util.Properties;
import java.util.Random;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

public class XSPlayer {

	public static void main(String[] a) throws Exception {
		new XSPlayer().run();
	}

	// basic setup

	private static float RATE = 44100;
	private static final double ROOT = pow(2, 1.0 / 12);
	private static final double A4_HZ = 440.0;
	private final Random rnd = new Random();
	private final SourceDataLine sourceDL;
	private final int TRACKS = 64;
	private final int BLOCKS = 256;
	private boolean inLoop = true;
	private boolean loopedMode = false;

	// general song params

	private int bpm = 140;
	private double masterVolume;
	private int stepsPerBeat = 16;

	// song structure

	private final String[][] blocksOfTrack = new String[TRACKS][];
	private final String[][] stepsOfBlock = new String[BLOCKS][];
	private final int[] instrumentOfTrack = new int[TRACKS];
	private final double[] volumeOfTrack = new double[TRACKS];

	// player state

	private final int[] curBlockIxOfTrack = new int[TRACKS];
	private final int[] curStepOfTrack = new int[TRACKS];
	private final long[] curSampleIxOfTrack = new long[TRACKS];
	private final int[] duration = new int[TRACKS];
	private final int[] durationProgress = new int[TRACKS];
	private final boolean[] newNote = new boolean[TRACKS];
	private final int[] noteLength = new int[TRACKS];

	// low and high pass filter states

	private final double[] curFreqOfTrack = new double[TRACKS];
	private final double[] lowPassCutoff = new double[TRACKS];
	private final double[] lastLowPassWetOfTrack = new double[TRACKS];
	private final double[] highPassCutoff = new double[TRACKS];
	private final double[] lastHighPassDryOfTrack = new double[TRACKS];
	private final double[] lastHighPassWetOfTrack = new double[TRACKS];

	// delay filter state

	private final double[][] delayBufferOfTrack = new double[TRACKS][(int) RATE]; // 1 sec
	private final int[] delayBufferPosOfTrack = new int[TRACKS];
	private final double[] delayOfTrack = new double[TRACKS];
	private final double[] delayRatioOfTrack = new double[TRACKS];

	// stereo width and pan filter

	private final double[] stereoWidthOfTrack = new double[TRACKS];
	private final double[] panOfTrack = new double[TRACKS];

	// tilt EQ

	private double tiltEqFrequency = 450; // 20-20khz
	private double tiltEqGain = 0; // -6 / +6 db
	private double[] lp_out = new double[2];

	// transpose

	private final int[] transposeOfTrack = new int[TRACKS];

	public XSPlayer() throws Exception {

		// open audio device

		AudioFormat format = new AudioFormat(RATE, 16, 2, true, false);
		sourceDL = AudioSystem.getSourceDataLine(format);
		sourceDL.open();
		sourceDL.start();

		// initialize state

		Arrays.fill(lowPassCutoff, 1);
		Arrays.fill(highPassCutoff, 1);
		Arrays.fill(curFreqOfTrack, Double.NaN);

		// load song file

		Properties p = new Properties();
		p.load(XSPlayer.class.getResourceAsStream("1.sng"));
		for (String k : p.stringPropertyNames()) {
			String[] v = p.getProperty(k).split("\\s+");
			switch (k.charAt(0)) {
				case 'T':
					blocksOfTrack[Integer.parseInt(k.substring(1))] = v;
					break;
				case 'B':
					stepsOfBlock[Integer.parseInt(k.substring(1))] = v;
					break;
				case 'M':
					masterVolume = Double.parseDouble(v[0]);
					break;
				case 'O':
					bpm = Integer.parseInt(v[0]);
					break;
				case 'N':
					stepsPerBeat = Integer.parseInt(v[0]);
					break;
				case 'P':
					loopedMode = Integer.parseInt(v[0]) == 1;
					inLoop = !loopedMode;
					break;
			}
		}

		Arrays.fill(duration, 1);
	}

	public void run() {
		byte[] buf = new byte[4 * (int) (RATE * 60 / bpm / stepsPerBeat)];

		// loop forever
		main: while (true) {
			int track = 0;
			while (track < TRACKS) {
				if (blocksOfTrack[track] == null) {
					track++;
					continue;
				}

				String curBlock = blocksOfTrack[track][curBlockIxOfTrack[track]];
				String val = curBlock.substring(1);
				switch (curBlock.charAt(0)) {

					case 'b':

						// evaluate step

						boolean ctrl;
						do {
							String step = stepsOfBlock[Integer.parseInt(val)][curStepOfTrack[track]];
							ctrl = false;

							switch (step.charAt(0)) {
								case 'X':
									curFreqOfTrack[track] = Double.NaN;
									break;
								case 'n':
									duration[track] = Integer.parseInt(step.substring(1));
									ctrl = true;
									break;
								case '-':
									break;
								default:
									// beginning of note
									if (durationProgress[track] == 0) {
										curSampleIxOfTrack[track] = 0;
										curFreqOfTrack[track] = getFrequency(step, transposeOfTrack[track]);
										newNote[track] = false;
										noteLength[track] = 0;
									}
							}

							noteLength[track]++;

							if (ctrl || ++durationProgress[track] == duration[track]) {
								durationProgress[track] = 0;

								// move position pointers for all tracks

								if (++curStepOfTrack[track] >= stepsOfBlock[Integer.parseInt(val)].length) {
									curStepOfTrack[track] = 0;
									if (++curBlockIxOfTrack[track] >= blocksOfTrack[track].length) {
										blocksOfTrack[track] = null;
									}
								}

								// peek if new note is coming
								step = stepsOfBlock[Integer.parseInt(val)][curStepOfTrack[track]];
								if (step.charAt(0) >= 'A') {
									newNote[track] = true;
								}
							}

						} while (ctrl);

						track++;
						continue;

					case 't':
						transposeOfTrack[track] = Integer.parseInt(val);
						break;

					case '>':
						if (loopedMode && !inLoop) {
							inLoop = true;
							continue;
						}
						break;

					case '<':
						if (loopedMode && inLoop) {
							inLoop = false;
							Arrays.fill(curBlockIxOfTrack, 0);
							Arrays.fill(curStepOfTrack, 0);
							track = 0;
							continue;
						}
						break;
					case 'v':
						volumeOfTrack[track] = Double.parseDouble(val);
						break;
					case 'i':
						instrumentOfTrack[track] = Integer.parseInt(val);
						break;
					case 'l':
						if (Double.parseDouble(val) >= 0) {
							// alpha input
							lowPassCutoff[track] = Double.parseDouble(val);
						} else {
							// cutoff frequency input
							// dt = 1 / RATE
							// RC = 1 / ( 2 * PI * f )
							// a = dt / ( RC + dt )
							lowPassCutoff[track] = 1 / RATE / (1 / (2 * PI * -Double.parseDouble(val)) + 1 / RATE);
						}
						break;
					case 'h':
						if (Double.parseDouble(val) >= 0) {
							// alpha input
							highPassCutoff[track] = Double.parseDouble(val);
						} else {
							// cutoff frequency input
							// dt = 1 / RATE
							// RC = 1 / ( 2 * PI * f )
							// a = RC / ( RC + dt )
							highPassCutoff[track] = 1 / (2 * PI * -Double.parseDouble(val))
									/ (1 / (2 * PI * -Double.parseDouble(val)) + 1 / RATE);
						}
						break;
					case 'p':
						panOfTrack[track] = Double.parseDouble(val);
						break;
					case 'w':
						stereoWidthOfTrack[track] = Double.parseDouble(val);
						break;
					case 'r':
						delayRatioOfTrack[track] = Double.parseDouble(val);
						break;
					case 'd':
						delayOfTrack[track] = Double.parseDouble(val);
						break;
					case 'e':
						tiltEqFrequency = Double.parseDouble(val);
						break;
					case 'g':
						tiltEqGain = Double.parseDouble(val);
						break;
					case 'q':
						sourceDL.drain();
						break main;
				}

				if (++curBlockIxOfTrack[track] >= blocksOfTrack[track].length) {
					blocksOfTrack[track] = null;
				}
			}

			// fill the buffer with data

			if (inLoop) {
				for (int i = 0; i < buf.length; i += 4) {
					double[] samples = getSampleFromMixer();

					// convert double signal to 16 bit signed integer

					for (int c = 0; c < 2; c++) {
						short s = (short) (Short.MAX_VALUE * limit(samples[c], 1.0));
						buf[i + c * 2] = (byte) s;
						buf[i + c * 2 + 1] = (byte) (s >> 8); // little Endian
					}
				}

				// write buffer to audio device

				sourceDL.write(buf, 0, buf.length);

				/*
				 * System.arraycopy(buf, 0, w, wi, min(buf.length, w.length - wi)); wi += buf.length; if (wi >=
				 * w.length) { AudioFormat format = new AudioFormat(RATE, 16, 2, true, false); AudioInputStream ais =
				 * new AudioInputStream(new ByteArrayInputStream(w), format, w.length); try { AudioSystem.write(ais,
				 * AudioFileFormat.Type.WAVE, new File("test.wav")); } catch (IOException e) { e.printStackTrace(); }
				 * System.exit(0); }
				 */

			}
		}

		/*
		 * sourceDL.drain(); sourceDL.stop(); sourceDL.close();
		 */
	}

	// byte[] w = new byte[3 * 60 * (int) RATE * 4];
	// int wi = 0;

	private double limit(double sample, double limit) {
		return max(min(sample, limit), -limit);
	}

	private double[] getSampleFromMixer() {
		double[] mix = new double[2];
		int pos;
		for (int track = 0; track < TRACKS; track++) {
			if (blocksOfTrack[track] == null)
				continue;
			double sample = 0;
			long i = curSampleIxOfTrack[track]++;
			double t = i / RATE;
			double f = curFreqOfTrack[track];
			if (!Double.isNaN(curFreqOfTrack[track]))
				switch (instrumentOfTrack[track]) {
					case 0:
						// kickdrum
						sample = release(t, 10) * sin(2 * PI * t * f * release(t, 20));
						break;
					case 1:
						// sawtooth
						sample = wSawtooth(i, f) - wSawtooth(i, f * 3) * 0.20;
						break;
					case 2:
						// rich pulse
						sample = wPulse(i, f, 0.11, 0.02);
						break;
					case 3:
						// hi-hat
						sample = release(t, 25) * wPulse(i, f, 0.26, 0.0002) * (i % 100 < 50 ? 1.0 : 0.0)
								+ release(t, 15) * (((i % 4) == 0) ? rnd.nextGaussian() : 0);
						break;
					case 33:
						// short hi-hat
						sample = release(t, 35) * wPulse(i, f, 0.26, 0.0002) * (i % 100 < 50 ? 1.0 : 0.0)
								+ release(t, 35) * (((i % 4) == 0) ? rnd.nextGaussian() : 0);
						break;
					case 4:
						// airpad
						double detune = 1.00293;
						sample = wPulse(i, f * 2 * detune, (0.5 - 0.2 * sin(2 * PI * t / 60 * bpm / 4)), 0.0001) * 0.03
								* release(t, 0.25);
						sample += wNoise(i, f * 40) * 0.004 * release(t, 0.5);
						sample += wSine(t, f);
						sample += 0.5 * wSine(t, f * 2);
						sample += 1.0 / 3.0 * wSine(t, f * 3);
						sample += wSine(t, f * detune);
						sample += 0.5 * wSine(t, f * detune * 2);
						sample += 1.0 / 3.0 * wSine(t, f * detune * 3);
						sample *= attack(t, 10) * 0.6;
						highPassCutoff[track] = attack(t, 10) * 0.7;
						delayOfTrack[track] = 0.2;
						delayRatioOfTrack[track] = 0.4;
						break;
					case 5:
						// snare
						f = 170;
						sample = (1.4 * wPulse(i, f * release(t, 0.01), 0.5, 0.002) + 0.5
								* wPulse(i, 2.0 * f * release(t, 0.01), 0.5, 0.002) + 0.5 * wSawtooth(i, 4.0 * f))
								* release(t, 20) + wNoise(i, 12000 * release(t, 2)) * release(t, 20);
						sample = sample * 0.5 + 5 * release(t, 2) * sin(2 * PI * t * 440 * release(t, 20));
						break;
					case 6:
						// lead voice
						sample = wSine(t, f + 0.2 * sin(2 * PI * t * 6));
						sample += 0.2 * wSawtooth(i, f / 2 + 0.2 * wSine(t, 6));
						sample += 0.2 * wTriangle(i, f * 2 + 0.2 * wSine(t, 6));
						sample += 0.02 * wSine(t, f * 3 + 0.2 * wSine(t, 6));
						sample *= attack(t, 90) * (0.4 + 0.15 * wSine(t, 6)) * release(t, 0.3);
						break;
					case 7:
						// guitar
						sample = wPulse(i, f, 0.3, 0.0000) * 0.19 * release(t, 3);
						sample += 0.6 * wTriangle(i, f * 1.00293) * release(t, 5);
						sample += 0.05 * wSawtooth(i, f * 3) * release(t, 5);
						sample += sample * wRing(t, f, 90) * 0.05;
						lowPassCutoff[track] = 0.8 * (0.2 + release(t, 30));
						break;
					case 9:
						// bass effect
						sample = wPulse(i, f, 0.2 + sin(0.3 * curStepOfTrack[track]) * 0.15, 0) * release(t, 5);
						panOfTrack[track] = 0.8 * sin(2 * PI * curStepOfTrack[track] / 64);
						break;
					case 10:
						// strings
						f += sin(t * 30) * 0.03;
						sample = attack(t, 10)
								* (wPulse(i, f, 0.3 + 0.12 * sin(t * 32), 0) + 0.4
										* wPulse(i, f * 1.00293, 0.3 + 0.12 * sin(t * 32), 0) + 0.4 * wSawtooth(i,
										f / 2)) * 0.33;
						lowPassCutoff[track] = 0.01 + attack(t, 0.9) * 0.3;
						break;
					case 11:
						// new lead
						sample = wSawtooth(i, f);
						sample *= attack(t, 1000) * release(t, 20);
						sample += attack(t, 100) * wTriangle(i, f + 0.2 * sin(t * 40)) * release(t, 0.7)
								* (1 + sin(t * 40));
						sample += attack(t, 100) * wTriangle(i, f * 2) * release(t, 10);
						sample += attack(t, 1000) * 0.63 * wTriangle(i, f * 3) * release(t, 10);
						sample += attack(t, 1000) * 0.33 * wTriangle(i, f * 5) * release(t, 10);
						sample += attack(t, 1000) * wNoise(i, f * 20) * release(t, 400);
						lowPassCutoff[track] = 0.25 - 0.04 * release(t, 20);
						break;
				}

			// apply quick release envelope to smooth clicks

			double tt = 60.0 / bpm / stepsPerBeat * noteLength[track];
			if (newNote[track] && t >= tt - 0.01) {
				sample *= (tt - t) / 0.01;
			}

			// low pass filter

			lastLowPassWetOfTrack[track] = lowPassCutoff[track] * sample + (1.0 - lowPassCutoff[track])
					* lastLowPassWetOfTrack[track];

			// high pass filter

			sample = highPassCutoff[track]
					* (lastHighPassWetOfTrack[track] + lastLowPassWetOfTrack[track] - lastHighPassDryOfTrack[track]);
			lastHighPassDryOfTrack[track] = lastLowPassWetOfTrack[track];
			lastHighPassWetOfTrack[track] = sample;

			// delay

			pos = delayBufferPosOfTrack[track] - (int) (RATE * delayOfTrack[track]);
			if (pos < 0)
				pos = delayBufferOfTrack[track].length + pos;
			sample = sample + delayBufferOfTrack[track][pos] * delayRatioOfTrack[track];

			delayBufferOfTrack[track][delayBufferPosOfTrack[track]] = sample;

			// stereo width

			pos = delayBufferPosOfTrack[track] - (int) (RATE * stereoWidthOfTrack[track]);
			if (pos < 0)
				pos = delayBufferOfTrack[track].length + pos;
			double sampleRight = delayBufferOfTrack[track][pos];

			if (++delayBufferPosOfTrack[track] >= delayBufferOfTrack[track].length)
				delayBufferPosOfTrack[track] = 0;

			// stereo pan

			if (panOfTrack[track] < 0)
				sampleRight *= 1.0 + panOfTrack[track];
			else
				sample *= 1.0 - panOfTrack[track];

			// mixer

			mix[0] += sample * volumeOfTrack[track]; // left
			mix[1] += sampleRight * volumeOfTrack[track]; // right
		}

		// tilt EQ

		double amp = 6.0 / log(2);
		double sr3 = 3.0 * RATE;

		double gfactor = 5;
		double g1, g2;
		if (tiltEqGain > 0) {
			g1 = -gfactor * tiltEqGain;
			g2 = tiltEqGain;
		} else {
			g1 = -tiltEqGain;
			g2 = gfactor * tiltEqGain;
		}

		double lgain = exp(g1 / amp) - 1;
		double hgain = exp(g2 / amp) - 1;

		double omega = 2 * PI * tiltEqFrequency;
		double n = 1 / (sr3 + omega);
		double a0 = 2 * omega * n;
		double b1 = (sr3 - omega) * n;

		for (pos = 0; pos < 2; pos++) {
			lp_out[pos] = a0 * mix[pos] + b1 * lp_out[pos];
			mix[pos] = mix[pos] + lgain * lp_out[pos] + hgain * (mix[pos] - lp_out[pos]);
		}

		// apply master volume

		mix[0] *= masterVolume;
		mix[1] *= masterVolume;

		return mix;
	}

	/**
	 * convert note (e.g. a4, a#4) to frequency.
	 */
	private static double getFrequency(String note, int transpose) {
		// number of notes relative to A
		int y = (int) (0.5 + 1.68 * (note.charAt(0) - 'A'));
		if (note.charAt(1) == '#')
			y++;
		int octave = note.charAt(note.length() - 1) - '0';
		if (y >= 3)
			octave--;
		// calculate frequency relative to A4
		return A4_HZ * pow(ROOT, octave * 12 + y - 4 * 12 + transpose);
	}

	private double wSawtooth(long i, double f) {
		return 2.0 * (i % (RATE / f)) / (RATE / f) - 1.0;
	}

	/**
	 * 
	 * @param i
	 * @param t
	 * @param f
	 * @param pw
	 *            pulse width (neutral=0.5, lower values for richer tone)
	 * @param pwm
	 *            linear pulse width modulation coefficient (neutral=0.0, 0.02 for dynamic modulation)
	 * @return
	 */
	private double wPulse(long i, double f, double pw, double pwm) {
		return i % (RATE / f) < ((RATE / f) * pw + i * pwm) ? 1.0 : -1.0;
	}

	private double r = 0;

	private double wNoise(long i, double f) {
		int z = (int) (RATE / f);
		if (z == 0 || i % z == 0) {
			r = rnd.nextGaussian();
		}
		return r;
	}

	private double wSine(double t, double f) {
		return sin(2.0 * PI * t * f);
	}

	private double wTriangle(long i, double f) {
		return 2 * Math.abs(wSawtooth(i, f)) - 1;
	}

	private double wRing(double t, double f, double mod) {
		return signum(sin(t * 2 * PI * f * mod));
	}

	private double attack(double t, double a) {
		return 1.0 - exp(-t * a);
	}

	private double release(double t, double r) {
		return exp(-t * r);
	}

	private double waveshaper(double x, double gain, double amount) {
		x *= gain;
		return x * (abs(x) + amount) / (pow(x, 2) + (amount - 1.0) * abs(x) + 1);
	}
}
